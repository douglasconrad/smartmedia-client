<?php
class SmartConfController extends Zend_Controller_Action {
	public function init() {
		if (! Zend_Auth::getInstance ()->hasIdentity ()) {
			$this->_redirect ( '/usuarios/' );
		}
	}
	public function indexAction() {
		// ler arquivo ini.
		$ini = new Zend_Config_Ini ( APPLICATION_PATH . '/configs/smart.conf', 'config', true );
		$ini = $ini->toArray ();
		$this->view->ini = $ini;
	}
	public function editAction() {
		$config = new Zend_Config_Ini ( APPLICATION_PATH . '/configs/smart.conf', 'config', true );
		
		$emp_nome = $config->emp_nome;
		$defaultmediaserver = $config->defaultmediaserver;
		$sshport = $config->sshport;
		$defaultuser = $config->defaultuser;
		$defaultpassword = $config->defaultpassword;
		$musictimestart = $config->musictimestart;
		$musictimeend = $config->musictimeend;
		$defaultvolume = $config->defaultvolume;
		
		$form = new Zend_Form ();
		
		$_emp_nome = new Zend_Form_Element_Text ( 'emp_nome' );
		$_emp_nome->setValue ( $emp_nome )->setAttrib ( 'id', 'emp_nome' )->setLabel ( 'Nome Empresa' );
		$form->addElement ( $_emp_nome );
		
		$_defaultmediaserver = new Zend_Form_Element_Text ( 'defaultmediaserver' );
		$_defaultmediaserver->setValue ( $defaultmediaserver )->setAttrib ( 'id', 'defaultmediaserver' )->setLabel ( 'Endereço IP ou nome DNS do Servidor padrão de mídia' );
		$form->addElement ( $_defaultmediaserver );
		
		$_sshport = new Zend_Form_Element_Text ( 'sshport' );
		$_sshport->setValue ( $sshport )->setAttrib ( 'id', 'sshport' )->setLabel ( 'porta ssh default' );
		$form->addElement ( $_sshport );
		
		$_defaultuser = new Zend_Form_Element_Text ( 'defaultuser' );
		$_defaultuser->setValue ( $defaultuser )->setAttrib ( 'id', 'defaultuser' )->setLabel ( 'usuário padrao no servidor de midia' );
		$form->addElement ( $_defaultuser );
		
		$_defaultpassword = new Zend_Form_Element_Text ( 'defaultpassword' );
		$_defaultpassword->setValue ( $defaultpassword )->setAttrib ( 'id', 'defaultpassword' )->setLabel ( 'senha padrao' );
		$form->addElement ( $_defaultpassword );
		
		$_musictimestart = new Zend_Form_Element_Text ( 'musictimestart' );
		$_musictimestart->setValue ( $musictimestart )->setAttrib ( 'id', 'musictimestart' )->setLabel ( 'horario de inicio das musicas' );
		$form->addElement ( $_musictimestart );
		
		$_musictimeend = new Zend_Form_Element_Text ( 'musictimeend' );
		$_musictimeend->setValue ( $musictimeend )->setAttrib ( 'id', 'musictimeend' )->setLabel ( 'horario de fim das musicas' );
		$form->addElement ( $_musictimeend );
		
		$_defaultvolume = new Zend_Form_Element_Text ( 'defaultvolume' );
		$_defaultvolume->setValue ( $defaultvolume )->setAttrib ( 'id', 'defaultvolume' )->setLabel ( 'volume default para as musicas' );
		$form->addElement ( $_defaultvolume );
		
		$form->addElement ( new Zend_Form_Element_Submit ( 'submit', array (
				'label' => 'Salvar',
				'class' => 'btn btn-primary',
				'decorators' => array (
						'ViewHelper' 
				) 
		) ) );
		
		$form->addElement ( new Zend_Form_Element_Button ( 'delete', array (
				'label' => 'Cancel',
				'id' => 'delete-button',
				'class' => 'btn',
				'attribs' => array('onclick'=>'location.href=/smart-conf/'),
				'decorators' => array (
						'ViewHelper' 
				) 
		) ) );
		
		$form->addDisplayGroup ( array (
				'submit',
				'cancel' 
		), 'submitButtons', array (
				'decorators' => array (
						'FormElements',
						array (
								'HtmlTag',
								array (
										'tag' => 'div',
										'class' => 'btn-group' 
								) 
						) 
				) 
		) );
		
		/*
		 * $submit = new Zend_Form_Element_Submit('Salvar'); $submit->setAttrib('class', 'btn btn-primary'); $done = new Zend_Form_Element_Button('Cancelar'); $done->setAttrib('class', 'btn') ->setAttrib('onclick', "location.href=/smart-conf/"); $form->addElement($submit); $form->addElement($done);
		 */
		/* $submit = new Zend_Form_Element_Submit ( 'Salvar' );
		$submit->setAttrib ( 'class', 'btn btn-primary' );
		$done = new Zend_Form_Element_Button ( 'Cancelar' );
		$done->setAttrib ( 'class', 'btn' )->setAttrib ( 'onclick', "location.href=/smart-conf/" );
		$form->addElement ( $submit );
		$form->addElement ( $done ); */
		
		if ($this->_request->getPost ()) {
			
			$isValid = $form->isValid ( $_POST );
			if ($isValid) {
				
				$config->emp_nome = $_POST ['emp_nome'];
				$config->defaultmediaserver = $_POST ['defaultmediaserver'];
				$config->sshport = $_POST ['sshport'];
				$config->defaultuser = $_POST ['defaultuser'];
				$config->defaultpassword = $_POST ['defaultpassword'];
				$config->musictimestart = $_POST ['musictimestart'];
				$config->musictimeend = $_POST ['musictimeend'];
				$config->defaultvolume = $_POST ['defaultvolume'];
				
				$writer = new Zend_Config_Writer_Ini ( array (
						'config' => $config,
						'filename' => APPLICATION_PATH . '/configs/smart.conf' 
				) );
				
				$writer->write ();
				$this->_redirect ( '/smart-conf/' );
			}
		}
		
		$this->view->form = $form;
	}
}

