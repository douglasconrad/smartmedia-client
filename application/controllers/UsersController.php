<?php
class UsersController extends Zend_Controller_Action {
	protected $userId;
	public function indexAction() {
		$form_xml = new Zend_Config_Xml ( '../application/forms/login.xml' );
		$form = new Zend_Form ( $form_xml );
		
		$submit = new Zend_Form_Element_Submit ( 'Enter' );
		$submit->setAttrib ( 'class', 'btn btn-info' );
		$form->addElement ( $submit );
		
		if ($this->_request->getPost ()) {
			
			$db = Zend_Registry::get ( 'db' );
			if ($_POST ['login_email'] == '') {
				$form->getElement ( 'usuario' )->addError ( 'Usuário não informada' );
			}
			if ($_POST ['password'] == '') {
				$form->getElement ( 'senha' )->addError ( 'Senha não informada' );
			}
			$usuario = $_POST ['usuario'];
			$hash = md5 ( $_POST ['senha'] );
			
			$authAdapter = new Zend_Auth_Adapter_DbTable ( $db );
			$authAdapter->setTableName ( 'usuarios' );
			$authAdapter->setIdentityColumn ( 'email' );
			$authAdapter->setCredentialColumn ( 'senha_md5' );
			
			$authAdapter->setIdentity ( $usuario );
			$authAdapter->setCredential ( $hash );
			
			$auth = Zend_Auth::getInstance ();
			$result = $auth->authenticate ( $authAdapter );
			
			switch ($result->getCode ()) {
				case Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND :
				case Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID :
					echo $this->view->message = $this->view->translate ( 'Usuário ou Senha inválidos' );
					
					break;
				case Zend_Auth_Result::SUCCESS :
					$auth->getStorage ()->write ( $result->getIdentity () );
					$this->_redirect ( '/' );
					break;
				default :
					echo $this->view->message = $this->view->translate ( 'Falha na autenticação' );
					break;
			}
		}
		
		$this->view->form = $form;
	}
	public function logoffAction() {
		if (Zend_Auth::getInstance ()->hasIdentity ()) {
			Zend_Auth::getInstance ()->clearIdentity ();
		}
		$this->_redirect ( "/" );
	}
}

